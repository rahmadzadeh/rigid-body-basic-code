function [T,Transl,Rx,Ry,Rz] = calculateTransformationMatrix(roll,pitch,yaw,tx,ty,tz,sequence)
%% ==============================================================
% input/output
%
% the input to this function is the rotation angles and the translation values
%roll : rotation about x axis (rad)
%pitch : rotation about y axis (rad)
%yaw : rotation about z axis (rad)
%translation values: tx, ty, tz
% sequence : is the sequence of the rotation in an string e.g. 'xyz'
%
%the output is the total homogeneous 3d transformation matrix and
%individual matrices
%Rx : rotation matrix around x axis
%Ry : rotation matrix around y axis
%Rz : rotation matrix around z axis
%
% ==============================================================
% the order matters:
%
% oxyz0 --- rot w.r.t frame 1 ---> oxyz1 --- rot w.r.t frame 2 ---> oxyz2
%              R01                                    R12
%                            R02 = R01 * R12
%
%  Also : if the order of the rotation is : 'xyz'
%  R = Rz * Ry * Rx
% ==============================================================
%
%
%if the point is firstly rotated about the x axis, then about the y and z axes
%the transformation matrix should be calculated such as : T = Rz*Ry*Rx
%
% Rx = [1 0 0;0 cos(theta) -sin(theta);0 sin(theta) cos(theta)];
% Ry = [cos(theta) 0 sin(theta);0 1 0;-sin(theta) 0 cos(theta)];
% Rz = [cos(theta) -sin(theta) 0;sin(theta) cos(theta) 0;0 0 1];
% Pold = R * Pnew
%
% ==============================================================
%coded by: Reza Ahmadzadeh (reza.ahmadzadeh@iit.it) - Jan/2014
%to check out an example run the accompanying test.m file
% ==============================================================
%
%% code
assert(nargin >= 6,'number of inputs is not correct!!!!');
% calculations
Rx = [1 0 0 0;0 cos(roll) -sin(roll) 0; 0 sin(roll) cos(roll) 0;0 0 0 1];
Ry = [cos(pitch) 0 sin(pitch) 0;0 1 0 0;-sin(pitch) 0 cos(pitch) 0;0 0 0 1];
Rz = [cos(yaw) -sin(yaw) 0 0;sin(yaw) cos(yaw) 0 0;0 0 1 0;0 0 0 1];
Transl = [1 0 0 tx;0 1 0 ty;0 0 1 tz;0 0 0 1];
% different cases
switch nargin
    case 7
        disp(['the sequence of the rotaions is 1-' sequence(1) ' 2-' sequence(2) ' 3-' sequence(3) ' 4-' sequence(4)]);
        Rtemp = [Rx Ry Rz Transl]; % 1 2 3 4
        T = eye(4);
        sv = [strfind(sequence,'x') strfind(sequence,'y') strfind(sequence,'z') strfind(sequence,'t')]; % yzxt 3 1 2 4
        for i = 1:4
            n = find(sv==i);
            T = (Rtemp(:,4*(n-1)+1:4*n) * T);
        end
    case 6
        disp('the default sequence of the rotaions is 1-x 2-y 3-z 4-t');
        T = (Transl * (Rz * (Ry * Rx)));
    otherwise
        disp('number of input arguments is not correct!!')
end
