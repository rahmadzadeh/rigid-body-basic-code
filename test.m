%% this code is written for testing the transformation matrix
%
% We first create a cube in 3d space, then we try to translate and rotate
% it. we then check that the function is working correctly.
%
% code: Reza Ahmadzadeh (reza.ahmadzadeh@iit.it) - Jan/2014
%
clc, clear all, close all
%% plot a cube 
cube1 = [3 3 0 0 0 3 3 0 0 0 3 3 3 3 3 0 0;
    0 2 2 0 0 0 2 2 0 0 0 0 0 2 2 2 2;
    0 0 0 0 1 1 1 1 1 0 0 1 0 0 1 1 0;
    1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1];

figure;
plot3(cube1(1,:),cube1(2,:),cube1(3,:));hold on;
plot3(10,10,10);
xlabel('X');ylabel('Y');zlabel('Z');

%% specify the angles and the translation values
roll = 45 * pi/180; % roll/x
pitch = 90 * pi/180;  % pitch/y
yaw = 90 * pi/180; % yaw/z
tx = 4;
ty = 4;
tz = 4;
%% calculate the 3d rotation, translation, and transformation matrices
[Transf,transl,rotx,roty,rotz] = calculateTransformationMatrix(roll,pitch,yaw,tx,ty,tz,'tyzx');
% [Transf,transl,rotx,roty,rotz] = calculateTransformationMatrix(roll,pitch,yaw,tx,ty,tz);

%% lets show the transformation step by step
% let's do a yzxt transformation, manually
cube2 = transl*cube1;
plot3(cube2(1,:),cube2(2,:),cube2(3,:),'r');hold on;

cube3 = roty * cube2;
plot3(cube3(1,:),cube3(2,:),cube3(3,:),'g');hold on;

cube4 = rotz * cube3;
plot3(cube4(1,:),cube4(2,:),cube4(3,:),'c');hold on;

cube5 = rotx * cube4;
plot3(cube5(1,:),cube5(2,:),cube5(3,:),'m');hold on;

tt = (rotx * ( rotz * ( roty * ( transl))));

%% do it in one move
cube6 = (rotx * ( rotz * ( roty * ( transl * cube1))));
plot3(cube6(1,:),cube6(2,:),cube6(3,:),'k');hold on;

%% now let's use the Transformation matrix
cube7 = Transf * cube1;
plot3(cube7(1,:),cube7(2,:),cube7(3,:),'rp');grid

disp('the error between two matrices is:');
disp(Transf - tt);